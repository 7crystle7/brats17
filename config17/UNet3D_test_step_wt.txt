[data]
data_root         = /home/neuron/PycharmProjects/brats17/data_root/BraTS17Training
save_folder       = result17-test40-ugcn3d-hgg-nogcn-311upfeature-2e2
data_names        = config17/test_names_40_hgg.txt
modality_postfix  = [flair, t1, t1ce, t2]
file_postfix      = nii.gz

[network1]
net_type            = UGCN3DOG1
net_name            = UGCN3DOG1_WT32
downsample_twice    = True
data_shape          = [96, 96, 96, 4]
label_shape         = [96, 96, 96, 1]
class_num           = 4
model_file          = model17/UGCN1_train_87_2e2_k9_og_100_all_311upfeature_nogcn_8700.ckpt

[network1sg]
net_type            = UNet3D
net_name            = UNet3D_WT32
downsample_twice    = True
data_shape          = [96, 96, 96, 4]
label_shape         = [8, 8, 8, 1]
class_num           = 4
model_file          = model17/UNet3D_wtall1_9600.ckpt

[network1cr]
net_type            = UNet3D
net_name            = UNet3D_WT32
downsample_twice    = True
data_shape          = [96, 96, 96, 4]
label_shape         = [8, 8, 8, 1]
class_num           = 4
model_file          = model17/UNet3D_wtall1_9600.ckpt

[testing]
test_slice_direction = all
whole_tumor_only     = True
