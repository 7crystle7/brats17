# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

from niftynet.layer import layer_util
from niftynet.layer.base_layer import TrainableLayer
#from niftynet.layer.convolution import ConvolutionalLayer

from niftynet.layer.activation import ActiLayer
from niftynet.layer.bn import BNLayer
from niftynet.layer.convolution import ConvLayer, ConvolutionalLayer, BNReLuConvLayer

from niftynet.layer.deconvolution import DeconvolutionalLayer
from niftynet.layer.downsample import DownSampleLayer
from niftynet.layer.elementwise import ElementwiseLayer
from niftynet.layer.crop import CropLayer
from niftynet.utilities.util_common import look_up_operations


class UGCN3D(TrainableLayer):
    """
    reimplementation of 3D U-net
      Çiçek et al., "3D U-Net: Learning dense Volumetric segmentation from
      sparse annotation", MICCAI '16
    """

    def __init__(self,
                 num_classes,
                 w_initializer=None,
                 w_regularizer=None,
                 b_initializer=None,
                 b_regularizer=None,
                 acti_func='prelu',
                 name='UGCN3D'):
        super(UGCN3D, self).__init__(name=name)

        self.n_features = [16, 32, 64, 128, 256, 512]
        # Enough memory start
        # self.n_features = [32, 64, 128, 256, 512]
        # Enough memory end
        self.acti_func = acti_func
        self.num_classes = num_classes

        self.initializers = {'w': w_initializer, 'b': b_initializer}
        self.regularizers = {'w': w_regularizer, 'b': b_regularizer}

        print('using {}'.format(name))


    def layer_op(self, images, is_training, layer_id=-1):
        # image_size  should be divisible by 8
        assert layer_util.check_spatial_dims(images, lambda x: x % 16 == 0)
        assert layer_util.check_spatial_dims(images, lambda x: x >= 89)
        print('the size of images', images)
        # block_layer = UNetBlock('DOWNSAMPLE',
        #                         (self.n_features[0], self.n_features[1]),
        #                         (3, 3), with_downsample_branch=True,
        #                         w_initializer=self.initializers['w'],
        #                         w_regularizer=self.regularizers['w'],
        #                         acti_func=self.acti_func,
        #                         name='L1')

        block_layer = SingleBlock('DOWNSAMPLE',
                                self.n_features[0],
                                5, with_downsample_branch=False,
                                w_initializer=self.initializers['w'],
                                w_regularizer=self.regularizers['w'],
                                acti_func=self.acti_func,
                                name='S1')

        images = block_layer(images, is_training)

        block_layer = EncoderBlock('DOWNSAMPLE',
                                (self.n_features[0], self.n_features[1]),
                                [[1, 3, 3], [1, 3, 3], [3, 1, 1]], with_downsample_branch=True,
                                w_initializer=self.initializers['w'],
                                w_regularizer=self.regularizers['w'],
                                acti_func=self.acti_func,
                                name='E1')


        pool_1, conv_1 = block_layer(images, is_training)
        print('the size of conv_1', conv_1)
        print('the size of pool_1', pool_1)
        print(block_layer)


        # block_layer = UNetBlock('DOWNSAMPLE',
        #                         (self.n_features[1], self.n_features[2]),
        #                         (3, 3), with_downsample_branch=True,
        #                         w_initializer=self.initializers['w'],
        #                         w_regularizer=self.regularizers['w'],
        #                         acti_func=self.acti_func,
        #                         name='L2')

        block_layer = EncoderBlock('DOWNSAMPLE',
                                   (self.n_features[1], self.n_features[2]),
                                   [[1, 3, 3], [1, 3, 3], [3, 1, 1]], with_downsample_branch=True,
                                   w_initializer=self.initializers['w'],
                                   w_regularizer=self.regularizers['w'],
                                   acti_func=self.acti_func,
                                   name='E2')


        pool_2, conv_2 = block_layer(pool_1, is_training)
        print('the size of conv_2', conv_2)
        print('the size of pool_2', pool_2)
        print(block_layer)



        # block_layer = UNetBlock('DOWNSAMPLE',
        #                         (self.n_features[2], self.n_features[3]),
        #                         (3, 3), with_downsample_branch=True,
        #                         w_initializer=self.initializers['w'],
        #                         w_regularizer=self.regularizers['w'],
        #                         acti_func=self.acti_func,
        #                         name='L3')


        block_layer = EncoderBlock('DOWNSAMPLE',
                                   (self.n_features[2], self.n_features[3]),
                                   [[1, 3, 3], [1, 3, 3], [3, 1, 1]], with_downsample_branch=True,
                                   w_initializer=self.initializers['w'],
                                   w_regularizer=self.regularizers['w'],
                                   acti_func=self.acti_func,
                                   name='E3')

        pool_3, conv_3 = block_layer(pool_2, is_training)
        print('the size of conv_3', conv_3)
        print('the size of pool_3', pool_3)
        print(block_layer)





        # block_layer = UNetBlock('DOWNSAMPLE',
        #                         (self.n_features[3], self.n_features[4]),
        #                         (3, 3), with_downsample_branch=True,
        #                         w_initializer=self.initializers['w'],
        #                         w_regularizer=self.regularizers['w'],
        #                         acti_func=self.acti_func,
        #                         name='L4')

        block_layer = EncoderBlock('DOWNSAMPLE',
                                   (self.n_features[3], self.n_features[4]),
                                   [[1, 3, 3], [1, 3, 3], [3, 1, 1]], with_downsample_branch=True,
                                   w_initializer=self.initializers['w'],
                                   w_regularizer=self.regularizers['w'],
                                   acti_func=self.acti_func,
                                   name='E4')




        pool_4, conv_4 = block_layer(pool_3, is_training)
        print('the size of pool_4', pool_4)
        print('the size of conv_4', conv_4)
        print(block_layer)







        # block_layer = UNetBlock('UPSAMPLE',
        #                         (self.n_features[4], self.n_features[5]),
        #                         (3, 3), with_downsample_branch=False,
        #                         w_initializer=self.initializers['w'],
        #                         w_regularizer=self.regularizers['w'],
        #                         acti_func=self.acti_func,
        #                         name='L5')

        block_layer = EncoderBlock('UPSAMPLE',
                                   (self.n_features[4], self.n_features[5]),
                                   [[1, 3, 3], [1, 3, 3], [3, 1, 1]], with_downsample_branch=False,
                                   w_initializer=self.initializers['w'],
                                   w_regularizer=self.regularizers['w'],
                                   acti_func=self.acti_func,
                                   name='E5')


        up_4, _ = block_layer(pool_4, is_training)
        print('the size of up_4', up_4)
        print(block_layer)




        # block_layer = UNetBlock('UPSAMPLE',
        #                         (self.n_features[4], self.n_features[4]),
        #                         (3, 3), with_downsample_branch=False,
        #                         w_initializer=self.initializers['w'],
        #                         w_regularizer=self.regularizers['w'],
        #                         acti_func=self.acti_func,
        #                         name='R4')

        block_layer = DecoderBlock ('UPSAMPLE',
                                self.n_features[3],
                                [[3, 3, 3],[3, 3, 3]], with_downsample_branch=False,
                                w_initializer=self.initializers['w'],
                                w_regularizer=self.regularizers['w'],
                                acti_func=self.acti_func,
                                name='D4')

        gc_layer = GCLayer(n_output_chns=self.n_features[3],
                           kernel_size=9,
                           w_initializer=self.initializers['w'],
                           w_regularizer=self.regularizers['w'],
                           name='GCM4')





        concat_4 = ElementwiseLayer('SUM')(conv_4, up_4)

        gcm_4 = gc_layer(concat_4, is_training)
        print('the size of gcm_4', gcm_4)
        print(gc_layer)


        #concat_4 = ElementwiseLayer('CONCAT')(conv_4, up_4)
        up_3, _ = block_layer(gcm_4, is_training)
        print('the size of concat_4 is ', concat_4)
        print('the size of up_3 is ', up_3)
        print(block_layer)

        # block_layer = UNetBlock('UPSAMPLE',
        #                         (self.n_features[3], self.n_features[3]),
        #                         (3, 3), with_downsample_branch=False,
        #                         w_initializer=self.initializers['w'],
        #                         w_regularizer=self.regularizers['w'],
        #                         acti_func=self.acti_func,
        #                         name='R3')

        block_layer = DecoderBlock('UPSAMPLE',
                                   self.n_features[2],
                                   [[3, 3, 3],[3, 3, 3]], with_downsample_branch=False,
                                   w_initializer=self.initializers['w'],
                                   w_regularizer=self.regularizers['w'],
                                   acti_func=self.acti_func,
                                   name='D3')

        gc_layer = GCLayer(n_output_chns=self.n_features[2],
                           kernel_size=9,
                           w_initializer=self.initializers['w'],
                           w_regularizer=self.regularizers['w'],
                           name='GCM3')







        concat_3 = ElementwiseLayer('SUM')(conv_3, up_3)
        #concat_3 = ElementwiseLayer('CONCAT')(conv_3, up_3)
        gcm_3 = gc_layer(concat_3, is_training)
        print('the size of gcm_3', gcm_3)
        print(gc_layer)

        up_2, _ = block_layer(gcm_3, is_training)
        print('the size of concat_3 is ', concat_3)
        print('the size of up_2 is ', up_2)
        print(block_layer)

        # block_layer = UNetBlock('UPSAMPLE',
        #                         (self.n_features[2], self.n_features[2]),
        #                         (3, 3), with_downsample_branch=False,
        #                         w_initializer=self.initializers['w'],
        #                         w_regularizer=self.regularizers['w'],
        #                         acti_func=self.acti_func,
        #                         name='R2')

        block_layer = DecoderBlock('UPSAMPLE',
                                   self.n_features[1],
                                   [[3, 3, 3],[3, 3, 3]], with_downsample_branch=False,
                                   w_initializer=self.initializers['w'],
                                   w_regularizer=self.regularizers['w'],
                                   acti_func=self.acti_func,
                                   name='D2')

        gc_layer = GCLayer(n_output_chns=self.n_features[1],
                           kernel_size=9,
                           w_initializer=self.initializers['w'],
                           w_regularizer=self.regularizers['w'],
                           name='GCM2')





        concat_2 = ElementwiseLayer('SUM')(conv_2, up_2)
       # concat_2 = ElementwiseLayer('CONCAT')(conv_2, up_2)
        gcm_2 = gc_layer(concat_2, is_training)
        print('the size of gcm_2', gcm_2)
        print(gc_layer)


        up_1, _ = block_layer(gcm_2, is_training)
        print('the size of concat_2 is ', concat_2)
        print('the size of up1 is ', up_1)
        print(block_layer)

        block_layer = DecoderBlock('None',
                                   self.n_features[0],
                                   [[3, 3, 3],[3, 3, 3]], with_downsample_branch=True,
                                   w_initializer=self.initializers['w'],
                                   w_regularizer=self.regularizers['w'],
                                   acti_func=self.acti_func,
                                   name='D1')


        gc_layer = GCLayer(n_output_chns=self.n_features[0],
                           kernel_size=9,
                           w_initializer=self.initializers['w'],
                           w_regularizer=self.regularizers['w'],
                           name='GCM1')




        concat_1 = ElementwiseLayer('SUM')(conv_1, up_1)
        gcm_1 = gc_layer(concat_1, is_training)
        print('the size of gcm_1', gcm_1)
        print(gc_layer)


       # concat_1 = ElementwiseLayer('CONCAT')(conv_1, up_1)
        _ ,up_final  = block_layer(gcm_1, is_training)
        print('the size of concat_1 is ', concat_1)
        print('the size of upfinal is ', up_final)
        print(block_layer)

        block_layer = SingleBlock('NONE',
                                 self.num_classes,
                                 1,
                                with_downsample_branch=True,
                                w_initializer=self.initializers['w'],
                                w_regularizer=self.regularizers['w'],
                                acti_func=self.acti_func,
                                name='R1_FC')


        # for the last layer, upsampling path is not used
        output_tensor = block_layer(up_final, is_training)
        print('output_tensor before cropping', output_tensor)

        # crop_layer = CropLayer(border=44, name='crop-88')
        # output_tensor = crop_layer(output_tensor)
        print('output tensor after cropping', output_tensor)
        print(block_layer)
        return output_tensor



SUPPORTED_OP = set(['DOWNSAMPLE', 'UPSAMPLE', 'BOTTOM','NONE'])


class UNetBlock(TrainableLayer):
    def __init__(self,
                 func,
                 n_chns,
                 kernels,
                 w_initializer=None,
                 w_regularizer=None,
                 with_downsample_branch=False,
                 acti_func='relu',
                 name='UNet_block'):

        super(UNetBlock, self).__init__(name=name)

        self.func = look_up_operations(func.upper(), SUPPORTED_OP)

        self.kernels = kernels
        self.n_chns = n_chns
        self.with_downsample_branch = with_downsample_branch
        self.acti_func = acti_func

        self.initializers = {'w': w_initializer}
        self.regularizers = {'w': w_regularizer}

    def layer_op(self, input_tensor, is_training):
        output_tensor = input_tensor
        for (kernel_size, n_features) in zip(self.kernels, self.n_chns):
            conv_op = ConvolutionalLayer(n_output_chns=n_features,
                                         kernel_size=kernel_size,
                                         w_initializer=self.initializers['w'],
                                         w_regularizer=self.regularizers['w'],
                                         acti_func=self.acti_func,
                                         name='conv_{}'.format(n_features))
            output_tensor = conv_op(output_tensor, is_training)

        if self.with_downsample_branch:
            branch_output = output_tensor
        else:
            branch_output = None

        if self.func == 'DOWNSAMPLE':
            downsample_op = DownSampleLayer('MAX',
                                            kernel_size=2,
                                            stride=2,
                                            name='down_2x2')
            output_tensor = downsample_op(output_tensor)
        elif self.func == 'UPSAMPLE':
            upsample_op = DeconvolutionalLayer(n_output_chns=self.n_chns[-1],
                                               kernel_size=2,
                                               stride=2,
                                               name='up_2x2')
            output_tensor = upsample_op(output_tensor, is_training)


        elif self.func == 'NONE':
            pass  # do nothing
        return output_tensor, branch_output


class SingleBlock(TrainableLayer):
    def __init__(self,
                 func,
                 n_chns,
                 kernels,
                 w_initializer=None,
                 w_regularizer=None,
                 with_downsample_branch=False,
                 acti_func='relu',
                 name='UNet_block'):

        super(SingleBlock, self).__init__(name=name)

        self.func = look_up_operations(func.upper(), SUPPORTED_OP)

        self.kernels = kernels
        self.n_chns = n_chns
        self.with_downsample_branch = with_downsample_branch
        self.acti_func = acti_func

        self.initializers = {'w': w_initializer}
        self.regularizers = {'w': w_regularizer}

    def layer_op(self, input_tensor, is_training):
        output_tensor = input_tensor
        n_features = self.n_chns
        kernel_size = self.kernels
       # for (kernel_size, n_features) in zip(self.kernels, self.n_chns):
        conv_op = ConvolutionalLayer(n_output_chns=n_features,
                                         kernel_size=kernel_size,
                                         w_initializer=self.initializers['w'],
                                         w_regularizer=self.regularizers['w'],
                                         acti_func=self.acti_func,
                                         name='conv_{}'.format(n_features))
        output_tensor = conv_op(output_tensor, is_training)

        # if self.with_downsample_branch:
        #     branch_output = output_tensor
        # else:
        #     branch_output = None
        #
        # if self.func == 'DOWNSAMPLE':
        #     downsample_op = DownSampleLayer('MAX',
        #                                     kernel_size=2,
        #                                     stride=2,
        #                                     name='down_2x2')
        #     output_tensor = downsample_op(output_tensor)
        # elif self.func == 'UPSAMPLE':
        #     upsample_op = DeconvolutionalLayer(n_output_chns=self.n_chns[-1],
        #                                        kernel_size=2,
        #                                        stride=2,
        #                                        name='up_2x2')
        #     output_tensor = upsample_op(output_tensor, is_training)
        #
        #
        # elif self.func == 'NONE':
        #     pass  # do nothing
        return output_tensor


class DecoderBlock(TrainableLayer):
    def __init__(self,
                 func,
                 n_chns,
                 kernels,
                 w_initializer=None,
                 w_regularizer=None,
                 with_downsample_branch=False,
                 acti_func='relu',
                 name='Decoder_block'):

        super(DecoderBlock, self).__init__(name=name)

        #print('new res encoder is done')

        self.func = look_up_operations(func.upper(), SUPPORTED_OP)

        self.kernels_1 = kernels[0]
        self.kernels_2 = kernels[1]
        self.n_chns = n_chns
        self.with_downsample_branch = with_downsample_branch
        self.acti_func = acti_func

        self.initializers = {'w': w_initializer}
        self.regularizers = {'w': w_regularizer}

    def layer_op(self, input_tensor, is_training):
        output_tensor = input_tensor

        kernel_size_1 = self.kernels_1
        kernel_size_2 = self.kernels_2
        n_features_in = output_tensor.shape[-1]
        n_features = self.n_chns

        #print('in channel num', n_features_in)

        print('The input feature is equal to the output feature in the decoder？', n_features_in == n_features)


        conv_op1 = ResBlock(n_features,
                     kernels=[kernel_size_1, kernel_size_1],
                     acti_func=self.acti_func,
                     w_initializer=self.initializers['w'],
                     w_regularizer=self.regularizers['w'],
                     name='{}'.format(n_features))


        # conv_op2 = BNReLuConvLayer(n_output_chns=n_features,
        #                              kernel_size=kernel_size_2,
        #                              w_initializer=self.initializers['w'],
        #                              w_regularizer=self.regularizers['w'],
        #                              acti_func=self.acti_func,
        #                              name='{}'.format(n_features))

        # gc_layer = GCLayer(n_output_chns=n_features_in,
        #                    kernel_size=3,
        #                    w_initializer=self.initializers['w'],
        #                    w_regularizer=self.regularizers['w'],
        #                    name='GCM')

        #output_tensor_res = gc_layer(output_tensor, is_training)
        #print('outputsensor for d is', output_tensor.shape)
        #print('ressensor for d is', output_tensor_res.shape)
        #output_tensor = ElementwiseLayer('SUM')(output_tensor,output_tensor_res)
        #print('result for d is', output_tensor.shape)

        # conv_op2 = BNReLuConvLayer(n_output_chns=n_features,
        #                              kernel_size=1,
        #                              w_initializer=self.initializers['w'],
        #                              w_regularizer=self.regularizers['w'],
        #                              acti_func=self.acti_func,
        #                              name='{}'.format(n_features))

        #output_tensor = conv_op2(output_tensor, is_training)
        #print('result for d is', output_tensor.shape)

        # conv_op2 = ResBlock(n_features,
        #              kernels=[kernel_size_2, kernel_size_2],
        #              acti_func=self.acti_func,
        #              w_initializer=self.initializers['w'],
        #              w_regularizer=self.regularizers['w'],
        #              name='{}'.format(n_features))

        output_tensor = conv_op1(output_tensor, is_training)
        #output_tensor = conv_op2(output_tensor, is_training)


        if self.with_downsample_branch:
            branch_output = output_tensor
        else:
            branch_output = None

        if self.func == 'DOWNSAMPLE':
            downsample_op = DownSampleLayer('MAX',
                                            kernel_size=2,
                                            stride=2,
                                            name='down_2x2')
            output_tensor = downsample_op(output_tensor)
        elif self.func == 'UPSAMPLE':
            upsample_op = DeconvolutionalLayer(n_output_chns=n_features,
                                               kernel_size=2,
                                               stride=2,
                                               name='up_2x2')
            output_tensor = upsample_op(output_tensor, is_training)
        elif self.func == 'NONE':
            pass  # do nothing
        return output_tensor, branch_output



class EncoderBlock(TrainableLayer):
    def __init__(self,
                 func,
                 n_chns,
                 kernels,
                 w_initializer=None,
                 w_regularizer=None,
                 with_downsample_branch=False,
                 acti_func='relu',
                 name='Encoder_block'):

        super(EncoderBlock, self).__init__(name=name)

        #print('new res encoder is done')

        self.func = look_up_operations(func.upper(), SUPPORTED_OP)

        self.kernels = kernels
        self.n_chns = n_chns
        self.with_downsample_branch = with_downsample_branch
        self.acti_func = acti_func

        self.initializers = {'w': w_initializer}
        self.regularizers = {'w': w_regularizer}

    def layer_op(self, input_tensor, is_training):
        output_tensor = input_tensor

        kernel_size1 = self.kernels[0]
        n_features1 = self.n_chns[0]

        kernel_size2 = self.kernels[1]
        n_features2 = self.n_chns[1]

        kernel_size3 = self.kernels[2]

        print('if the first feature for res equal to the input feature in each encoder', output_tensor.shape[-1]==n_features1)



        #for (kernel_size, n_features) in zip(self.kernels, self.n_chns):
            # conv_op = ConvolutionalLayer(n_output_chns=n_features,
            #                              kernel_size=kernel_size,
            #                              w_initializer=self.initializers['w'],
            #                              w_regularizer=self.regularizers['w'],
            #                              acti_func=self.acti_func,
            #                              name='{}'.format(n_features))

        conv_op1 = ResBlock(n_features1,
                     kernels=[kernel_size1, kernel_size1],
                     acti_func=self.acti_func,
                     w_initializer=self.initializers['w'],
                     w_regularizer=self.regularizers['w'],
                     name='{}'.format(n_features1))


        conv_311_1 = BNReLuConvLayer(n_output_chns=n_features1,
                                     kernel_size=kernel_size3,
                                     w_initializer=self.initializers['w'],
                                     w_regularizer=self.regularizers['w'],
                                     acti_func=self.acti_func,
                                     name='conv311_{}'.format(n_features1))

        conv_111 = BNReLuConvLayer(n_output_chns=n_features2,
                                   kernel_size=3,
                                   w_initializer=self.initializers['w'],
                                   w_regularizer=self.regularizers['w'],
                                   acti_func=self.acti_func,
                                   name='conv_{}'.format(n_features2))



        conv_op2 = ResBlock(n_features2,
                            kernels=[kernel_size2, kernel_size2],
                            acti_func=self.acti_func,
                            w_initializer=self.initializers['w'],
                            w_regularizer=self.regularizers['w'],
                            name='{}'.format(n_features2))

        conv_311_2 = BNReLuConvLayer(n_output_chns=n_features2,
                                     kernel_size=kernel_size3,
                                     w_initializer=self.initializers['w'],
                                     w_regularizer=self.regularizers['w'],
                                     acti_func=self.acti_func,
                                     name='conv311_{}'.format(n_features2))


        output_tensor = conv_op1(output_tensor, is_training)
        output_tensor = conv_311_1(output_tensor, is_training)

        output_tensor = conv_111(output_tensor, is_training)

        output_tensor = conv_op2(output_tensor, is_training)
        output_tensor = conv_311_2(output_tensor, is_training)


        if self.with_downsample_branch:
            branch_output = output_tensor
        else:
            branch_output = None

        if self.func == 'DOWNSAMPLE':
            downsample_op = DownSampleLayer('MAX',
                                            kernel_size=2,
                                            stride=2,
                                            name='down_2x2')
            output_tensor = downsample_op(output_tensor)
        elif self.func == 'UPSAMPLE':
            upsample_op = DeconvolutionalLayer(n_output_chns=self.n_chns[0],
                                               kernel_size=2,
                                               stride=2,
                                               name='up_2x2')
            output_tensor = upsample_op(output_tensor, is_training)
        #
        # elif self.func == 'BOTTOM':
        #
        #     gc_layer = GCLayer(n_output_chns=n_features2,
        #                        kernel_size=9,
        #                        w_initializer=self.initializers['w'],
        #                        w_regularizer=self.regularizers['w'],
        #                        name='GCM')
        #
        #     res_layer = ResBlock

        elif self.func == 'NONE':
            pass  # do nothing
        return output_tensor, branch_output


class ResBlock(TrainableLayer):
    """
    This class define a high-resolution block with residual connections
    kernels - specify kernel sizes of each convolutional layer
            - e.g.: kernels=(5, 5, 5) indicate three conv layers of kernel_size 5
    with_res - whether to add residual connections to bypass the conv layers
    """

    def __init__(self,
                 n_output_chns,
                 kernels=[[1, 3, 3], [1, 3, 3]],
                 acti_func='relu',
                 w_initializer=None,
                 w_regularizer=None,
                 with_res=True,
                 name='ResBlock'):
        super(ResBlock, self).__init__(name=name)
        self.n_output_chns = n_output_chns
        if hasattr(kernels, "__iter__"):  # a list of layer kernel_sizes
            self.kernels = kernels
        else:  # is a single number (indicating single layer)
            self.kernels = [kernels]
        self.acti_func = acti_func
        self.with_res = with_res

        self.initializers = {'w': w_initializer}
        self.regularizers = {'w': w_regularizer}

    def layer_op(self, input_tensor, is_training):
        output_tensor = input_tensor
        for i in range(len(self.kernels)):
            # create parameterised layers
            bn_op = BNLayer(regularizer=self.regularizers['w'],
                            name='bn_{}'.format(i))
            acti_op = ActiLayer(func=self.acti_func,
                                regularizer=self.regularizers['w'],
                                name='acti_{}'.format(i))
            conv_op = ConvLayer(n_output_chns=self.n_output_chns,
                                kernel_size=self.kernels[i],
                                w_initializer=self.initializers['w'],
                                w_regularizer=self.regularizers['w'],
                                name='conv_{}'.format(i))
            # connect layers
            output_tensor = bn_op(output_tensor, is_training)
            output_tensor = acti_op(output_tensor)
            output_tensor = conv_op(output_tensor)
        # make residual connections
        if self.with_res:
            output_tensor = ElementwiseLayer('SUM')(output_tensor, input_tensor)
        return output_tensor





class GCLayer(TrainableLayer):
    """
    This class define a high-resolution block with residual connections
    kernels - specify kernel sizes of each convolutional layer
            - e.g.: kernels=(5, 5, 5) indicate three conv layers of kernel_size 5
    with_res - whether to add residual connections to bypass the conv layers
    """

    def __init__(self,
                 n_output_chns,
                 kernel_size = 7,
                 acti_func='relu',
                 w_initializer=None,
                 w_regularizer=None,
                 with_res=True,
                 name='GCLayer'):
        super(GCLayer, self).__init__(name=name)
        self.n_output_chns = n_output_chns

        self.acti_func = acti_func
        self.with_res = with_res

        self.initializers = {'w': w_initializer}
        self.regularizers = {'w': w_regularizer}

        self.kernel = kernel_size

    def layer_op(self, input_tensor, is_training):


        kernel = self.kernel

        conv_l1 = ConvLayer(n_output_chns=self.n_output_chns,
                                kernel_size=[1, kernel, kernel],
                                w_initializer=self.initializers['w'],
                                w_regularizer=self.regularizers['w'],
                                name='conv_{}'.format('l1'))

        conv_l2 = ConvLayer(n_output_chns=self.n_output_chns,
                            kernel_size=[kernel, 1, 1],
                            w_initializer=self.initializers['w'],
                            w_regularizer=self.regularizers['w'],
                            name='conv_{}'.format('l2'))

        conv_r1 = ConvLayer(n_output_chns=self.n_output_chns,
                            kernel_size=[kernel, 1, 1],
                            w_initializer=self.initializers['w'],
                            w_regularizer=self.regularizers['w'],
                            name='conv_{}'.format('r1'))


        conv_r2 = ConvLayer(n_output_chns=self.n_output_chns,
                            kernel_size=[1, kernel, kernel],
                            w_initializer=self.initializers['w'],
                            w_regularizer=self.regularizers['w'],
                            name='conv_{}'.format('r2'))

            # connect layers

        output_tensor_l = conv_l1(input_tensor)
        output_tensor_l = conv_l2(output_tensor_l)

        output_tensor_r = conv_r1(input_tensor)
        output_tensor_r = conv_r2(output_tensor_r)
        # make residual connections

        output_tensor = ElementwiseLayer('SUM')(output_tensor_l, output_tensor_r)


        return output_tensor


